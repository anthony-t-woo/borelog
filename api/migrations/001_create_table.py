steps = [
    [
        """
        CREATE TABLE projects (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(1000) NOT NULL, 
            start_date DATE NOT NULL,
            completion_date DATE NOT NULL
        );
        """,
        """
        DROP TABLE projects
        """,
    ]
]
